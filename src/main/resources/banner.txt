${AnsiColor.208}${AnsiBackground.235}
${AnsiBackground.236}  ____    U _____ u  __     __      _      U _____ u     _        ____    U _____ u    ____       ____
${AnsiBackground.237} |  _"\   \| ___"|/  \ \   /"/u    |"|     \| ___"|/ U  /"\  u   |  _"\   \| ___"|/ U |  _"\ u   / __"| u
${AnsiBackground.238}/| | | |   |  _|"     \ \ / //   U | | u    |  _|"    \/ _ \/   /| | | |   |  _|"    \| |_) |/  <\___ \/
${AnsiBackground.239}U| |_| |\  | |___     /\ V /_,-.  \| |/__   | |___    / ___ \   U| |_| |\  | |___     |  _ <     u___) |
${AnsiBackground.240} |____/ u  |_____|   U  \_/-(_/    |_____|  |_____|  /_/   \_\   |____/ u  |_____|    |_| \_\    |____/>>
${AnsiBackground.239}  |||_     <<   >>     //          //  \\   <<   >>   \\    >>    |||_     <<   >>    //   \\_    )(  (__)
${AnsiBackground.238} (__)_)   (__) (__)   (__)        (_")("_) (__) (__) (__)  (__)  (__)_)   (__) (__)  (__)  (__)  (__)
${AnsiBackground.237}                                                                                 Book Library Data Service
${AnsiBackground.236}                                                                                 application.version
${AnsiBackground.235}                                                                                 (${application.version})
${AnsiBackground.DEFAULT}${AnsiColor.DEFAULT}