package com.devleaders.booklibrary.error;

public class UserOrBookDoesNotExistException extends RuntimeException {
    public UserOrBookDoesNotExistException(String message) {
        super(message);
    }
}
