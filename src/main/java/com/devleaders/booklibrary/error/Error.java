package com.devleaders.booklibrary.error;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

@Getter
public class Error {
    private Map<String, String> error;

    private Error(Map<String, String> error) {
        this.error = error;
    }

    public static Error createError(String message) {
        HashMap<String, String> error = new HashMap<>();
        error.put("message", message);
        return new Error(error);
    }
}
