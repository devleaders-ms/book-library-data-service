package com.devleaders.booklibrary.error;

public class UserDoesNotExistExeption extends RuntimeException {
    public UserDoesNotExistExeption(long id) {
        super("User with id: " + id + " does not exist!");
    }
}
