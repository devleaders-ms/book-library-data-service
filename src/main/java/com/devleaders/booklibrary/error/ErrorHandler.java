package com.devleaders.booklibrary.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static com.devleaders.booklibrary.error.Error.createError;

@RestControllerAdvice
public class ErrorHandler {

    @ExceptionHandler(UserOrBookDoesNotExistException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error userOrBookDoesNotExistException(UserOrBookDoesNotExistException exception) {
        return createError(exception.getMessage());
    }

    @ExceptionHandler(UserOrBookItemDoesNotExistException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error userOrBookItemDoesNotExistException(UserOrBookItemDoesNotExistException exception) {
        return createError(exception.getMessage());
    }

    @ExceptionHandler(BookItemDoesNotExistException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error bookItemDoesNotExistException(BookItemDoesNotExistException exception) {
        return createError(exception.getMessage());
    }

    @ExceptionHandler(UserDoesNotExistException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error userDoesNotExistException(UserDoesNotExistException exception) {
        return createError(exception.getMessage());
    }

    @ExceptionHandler(BookAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    public Error handle(BookAlreadyExistsException exception) {
        return createError(exception.getMessage());
    }

    @ExceptionHandler(BookNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public Error bookNotFoundException(BookNotFoundException exception) {
        return createError(exception.getMessage());
    }
}
