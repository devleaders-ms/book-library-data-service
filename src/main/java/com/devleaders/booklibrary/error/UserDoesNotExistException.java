package com.devleaders.booklibrary.error;

public class UserDoesNotExistException extends RuntimeException {
    public UserDoesNotExistException(long userId) {
        super("User with id: " + userId + " does not exist.");
    }
}
