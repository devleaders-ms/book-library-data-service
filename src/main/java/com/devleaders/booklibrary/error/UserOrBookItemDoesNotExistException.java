package com.devleaders.booklibrary.error;

public class UserOrBookItemDoesNotExistException extends RuntimeException {
    public UserOrBookItemDoesNotExistException(String message) {
        super(message);
    }
}
