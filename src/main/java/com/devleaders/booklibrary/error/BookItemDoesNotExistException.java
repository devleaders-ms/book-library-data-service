package com.devleaders.booklibrary.error;

public class BookItemDoesNotExistException extends RuntimeException {
    public BookItemDoesNotExistException(long id) {
        super("BookItem with id: " + id + " does not exist!");
    }
}
