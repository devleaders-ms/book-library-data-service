package com.devleaders.booklibrary.controller;

import com.devleaders.booklibrary.model.Book;
import com.devleaders.booklibrary.model.BookDto;
import com.devleaders.booklibrary.service.BookCrudService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/v1/api")
@Validated
@RequiredArgsConstructor
public class BookController {

    private final BookCrudService bookCrudService;

    @RequestMapping(path = "/books", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class)})
    @ApiOperation(value = "Create Book")
    public ResponseEntity<BookDto> create(@RequestBody BookDto bookDto) {
        BookDto newBook = bookCrudService.create(bookDto);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(newBook);
    }

    @RequestMapping(path = "/books", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class)})
    @ApiOperation(value = "Get all books")
    public ResponseEntity<List<BookDto>> getAll(@RequestParam(required = false) String authorName, @RequestParam(required = false) String title) {
        List<BookDto> books;
        if (authorName != null && title != null) {
            books = bookCrudService.getBooksByAuthorAndTitle(authorName, title);
        } else {
            books = Optional.ofNullable(authorName)
                    .map(bookCrudService::getBooksByAuthor)
                    .orElseGet(() -> Optional.ofNullable(title)
                            .map(bookCrudService::getBooksByTitleLike)
                            .orElseGet(bookCrudService::getAllBooks)
                    );
        }
        return ResponseEntity.ok().body(books);
    }

    @RequestMapping(path = "/books/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Read Book")
    public ResponseEntity<BookDto> read(@PathVariable long id) {
        return bookCrudService.read(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @RequestMapping(path = "/books/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Update Book")
    public ResponseEntity<BookDto> update(@RequestBody Book book, @PathVariable Long id) {
        return bookCrudService.update(book, id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @RequestMapping(path = "/books/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Delete Book")
    public ResponseEntity<?> delete(@PathVariable long id) {
        return bookCrudService.delete(id)
                ? ResponseEntity.status(HttpStatus.NO_CONTENT).build() : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

}
