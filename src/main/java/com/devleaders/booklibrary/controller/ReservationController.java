package com.devleaders.booklibrary.controller;

import com.devleaders.booklibrary.model.Reservation;
import com.devleaders.booklibrary.model.ReservationDto;
import com.devleaders.booklibrary.service.ReservationsService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("v1/api")
@Validated
@RequiredArgsConstructor
public class ReservationController {

    private final ReservationsService reservationsService;

    @RequestMapping(path = "/reservations", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class)})
    @ApiOperation(value = "Create Reservation")
    public ResponseEntity<Reservation> create(@RequestBody ReservationDto reservationDto) {
        Reservation newReservation = reservationsService.create(reservationDto);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(newReservation);
    }

    @RequestMapping(path = "/reservations", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class)})
    @ApiOperation(value = "Get all reservations")
    public ResponseEntity<List<Reservation>> getAll(@RequestParam(required = false) Long userId) {
        List<Reservation> reservations = Optional.ofNullable(userId)
                .map(reservationsService::getReservationsByUserId)
                .orElseGet(reservationsService::getAllReservations);
        return ResponseEntity.ok(reservations);
    }

    @RequestMapping(path = "/reservations/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Reservation wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Read reservation")
    public ResponseEntity<Reservation> read(@PathVariable long id) {
        return reservationsService.read(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @RequestMapping(path = "/reservations/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Reservation wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Update reservation")
    public ResponseEntity<Reservation> update(@RequestBody ReservationDto reservationDto, @PathVariable Long id) {
        return reservationsService.update(reservationDto, id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @RequestMapping(path = "/reservations/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Reservation wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Delete reservation")
    public ResponseEntity<?> delete(@PathVariable long id) {
        return reservationsService.delete(id)
                ? ResponseEntity.status(HttpStatus.NO_CONTENT).build() : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }
}
