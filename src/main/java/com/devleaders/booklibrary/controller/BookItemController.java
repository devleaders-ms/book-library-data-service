package com.devleaders.booklibrary.controller;

import com.devleaders.booklibrary.model.BookItem;
import com.devleaders.booklibrary.service.BookItemCrudService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/v1/api")
@Validated
@RequiredArgsConstructor
public class BookItemController {

    private final BookItemCrudService bookItemCrudService;

    @RequestMapping(path = "/bookItems", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class)})
    @ApiOperation(value = "Create Book Item")
    public ResponseEntity<BookItem> create(@RequestBody BookItem bookItem) {
        BookItem newBookItem = bookItemCrudService.create(bookItem);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(newBookItem);
    }

    @RequestMapping(path = "/bookItems", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book Items wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Get all Book items")
    public ResponseEntity<List<BookItem>> getBookItems(@RequestParam(required = false) Long bookId) {
        List<BookItem> bookItems = bookId != null
                ? bookItemCrudService.getBookItemsByBookId(bookId)
                : bookItemCrudService.getAllBookItems();
        return ResponseEntity.ok().body(bookItems);
    }

    @RequestMapping(path = "/bookItems/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book item wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Read Book Item")
    public ResponseEntity<BookItem> read(@PathVariable long id) {
        return bookItemCrudService.read(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @RequestMapping(path = "/bookItems/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book item wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Update Book Item")
    public ResponseEntity<BookItem> update(@RequestBody BookItem bookItem, @PathVariable Long id) {
        return bookItemCrudService.update(bookItem, id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @RequestMapping(path = "/bookItems/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book item wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Delete Book Item")
    public ResponseEntity<?> delete(@PathVariable long id) {
        return bookItemCrudService.delete(id)
                ? ResponseEntity.status(HttpStatus.NO_CONTENT).build() : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(path = "/bookItems/{id}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book item wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Update Book Item")
    public ResponseEntity<?> patch(@RequestBody Map<String, Object> bookItemUpdates,
                                   @PathVariable("id") long id) {
        return bookItemCrudService.patch(bookItemUpdates, id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @RequestMapping(path = "/bookItems", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, params = {"bookId", "isAvailable"})
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Get available book items by book id")
    public ResponseEntity<List<BookItem>> getAvailableBookItems(@RequestParam Long bookId, @RequestParam String isAvailable) {
        return ResponseEntity.ok(bookItemCrudService.getAvailableBookItems(bookId, Boolean.valueOf(isAvailable)));
    }
}
