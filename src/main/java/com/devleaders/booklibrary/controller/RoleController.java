package com.devleaders.booklibrary.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/swag")
public class RoleController {

    @PostMapping
    public String isWorking() {
        return "Swagger works";
    }
}