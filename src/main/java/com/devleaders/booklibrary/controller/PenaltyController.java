package com.devleaders.booklibrary.controller;

import com.devleaders.booklibrary.model.Penalty;
import com.devleaders.booklibrary.model.PenaltyDto;
import com.devleaders.booklibrary.service.PenaltyService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("v1/api")
@Validated
@RequiredArgsConstructor
public class PenaltyController {

    private final PenaltyService penaltyService;

    @PostMapping(path = "/penalties", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 400, message = "User or BookItem doesn't exist", response = ResponseStatus.class)})
    @ApiOperation(value = "Create penalty")
    public ResponseEntity<Penalty> create(@RequestBody PenaltyDto penaltyDto) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(penaltyService.create(penaltyDto));
    }

    @GetMapping(path = "/penalties", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(path = "/penalties", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Penalty wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Get all penalties")
    public ResponseEntity<List<Penalty>> getAll() {
        return ResponseEntity.ok(penaltyService.readAll());
    }

    @GetMapping(path = "/penalties/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(path = "/penalties/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Penalty wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Get penalty by id")
    public ResponseEntity<Penalty> getById(@PathVariable long id) {
        return penaltyService.readById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @GetMapping(path = "/users/{userId}/penalties", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(path = "/users/{userId}/penalties", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Penalty wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Get all penalties by user id")
    public ResponseEntity<List<Penalty>> getAllByUserId(@PathVariable long userId) {
        return penaltyService.readAllByUserId(userId)
                .filter(penalties -> !penalties.isEmpty())
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @GetMapping(path = "/book-items/{bookItemId}/penalties", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(path = "/book-items/{bookItemId}/penalties", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Penalty wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Get all penalties by book item id")
    public ResponseEntity<List<Penalty>> getAllByBookItemId(@PathVariable long bookItemId) {
        return penaltyService.readAllByBookItemId(bookItemId)
                .filter(penalties -> !penalties.isEmpty())
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }


    @DeleteMapping(path = "/penalties/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Penalty wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Delete penalty")
    public ResponseEntity<?> delete(@PathVariable long id) {
        return penaltyService.delete(id)
                .filter(deleted -> deleted)
                .map(__ -> ResponseEntity.status(HttpStatus.NO_CONTENT).build())
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @PutMapping(path = "/penalties/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Penalty wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Update penalty")
    public ResponseEntity<Penalty> update(@RequestBody PenaltyDto penaltyDto, @PathVariable("id") Long id) {
        return penaltyService.update(penaltyDto, id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @PatchMapping(path = "/penalties/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Penalty wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Patch penalty")
    public ResponseEntity<Penalty> patch(@RequestBody Map<String, Object> penaltyUpdates, @PathVariable("id") long id) {
        return penaltyService.patch(penaltyUpdates, id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }
}
