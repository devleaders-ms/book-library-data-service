package com.devleaders.booklibrary.controller;

import com.devleaders.booklibrary.mapper.UserMapper;
import com.devleaders.booklibrary.model.User;
import com.devleaders.booklibrary.model.UserDto;
import com.devleaders.booklibrary.service.ReservationsService;
import com.devleaders.booklibrary.service.UserCrudService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/v1/api")
@Validated
@RequiredArgsConstructor
public class UserController {

    private final UserCrudService userCrudService;
    private final ReservationsService reservationsService;
    private final UserMapper userMapper;

    @RequestMapping(path = "/users", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class)})
    @ApiOperation(value = "Create User")
    public ResponseEntity<UserDto> create(@RequestBody UserDto userDto) {
        User user = userMapper.mapDtoToUser(userDto);
        User createdUser = userCrudService.create(user);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(userMapper.mapUserToDto(createdUser));
    }

    @RequestMapping(path = "/users/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "User wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Read User")
    public ResponseEntity<UserDto> read(@PathVariable("id") long id) {
        return userCrudService.read(id)
                .map(userMapper::mapUserToDto)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @RequestMapping(path = "/users", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "User wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Read User")
    public ResponseEntity<?> read(@RequestParam(value = "login", required = false) String login,
                                  @RequestParam(value = "fullName", required = false) String fullName) {
        if (login != null) {
            return userCrudService.read(login)
                    .map(userMapper::mapUserToDto)
                    .map(ResponseEntity::ok)
                    .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
        }
        List<UserDto> users = userCrudService.getUsersByFullName(fullName)
                .stream()
                .map(userMapper::mapUserToDto)
                .collect(Collectors.toList());
        return ResponseEntity.ok(users);
    }

    @RequestMapping(path = "/users", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "User wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Update User")
    public ResponseEntity<UserDto> update(@RequestParam(value = "User: ") UserDto userDto) {
        User user = userMapper.mapDtoToUser(userDto);
        return userCrudService.update(user)
                .map(userMapper::mapUserToDto)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @RequestMapping(path = "/users/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "User wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Delete User")
    public ResponseEntity<UserDto> delete(@PathVariable("id") long id) {
        return userCrudService.delete(id)
                ? ResponseEntity.status(HttpStatus.NO_CONTENT).build()
                : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(path = "/users/{id}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "User wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Update User")
    public ResponseEntity<UserDto> patch(@RequestBody Map<String, String> userUpdates,
                                         @PathVariable("id") long id) {
        return userCrudService.patch(userUpdates, id)
                .map(userMapper::mapUserToDto)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }
}
