package com.devleaders.booklibrary.controller;

import com.devleaders.booklibrary.model.BookReview;
import com.devleaders.booklibrary.model.BookReviewDto;
import com.devleaders.booklibrary.service.BookReviewService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("v1/api")
@Validated
@Slf4j
@RequiredArgsConstructor
public class BookReviewController {

    private final BookReviewService bookReviewService;

    @RequestMapping(path = "/reviews", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 201, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 400, message = "User or Book doesn't exist", response = ResponseStatus.class)})
    @ApiOperation(value = "Create Book Review")
    public ResponseEntity<BookReview> create(@RequestBody BookReviewDto bookReviewDto) {
        BookReview bookReview = bookReviewService.create(bookReviewDto);
        return ResponseEntity.status(HttpStatus.CREATED).body(bookReview);
    }

    @RequestMapping(path = "/reviews/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book reviews wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Get all book reviews")
    public ResponseEntity<BookReview> getById(@PathVariable long id) {
        return bookReviewService.getById(id)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @RequestMapping(path = "/reviews", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - proper execution of service", response = ResponseStatus.class)})
    @ApiOperation(value = "Get all book reviews by book id or user id")
    public ResponseEntity<List<BookReview>> getReviews(@RequestParam(required = false) Long bookId,
                                                       @RequestParam(required = false) Long userId) {
        if (bookId != null && userId != null) {
            log.error("Request GET /reviews with both bookId and userId");
            return ResponseEntity.badRequest().build();
        }

        if (bookId == null && userId == null) {
            log.error("Request GET /reviews without bookId and without userId");
            return ResponseEntity.badRequest().build();
        }

        List<BookReview> reviews;
        if (bookId != null) {
            reviews = bookReviewService.getAllByBookId(bookId);
        } else {
            reviews = bookReviewService.getAllByUserId(userId);
        }

        return ResponseEntity.ok(reviews);
    }

    @RequestMapping(path = "/reviews/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Update Book review")
    public ResponseEntity<BookReview> update(@RequestBody BookReviewDto bookReviewDto, @PathVariable Long id) {
        return bookReviewService.update(bookReviewDto, id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @RequestMapping(path = "/reviews/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book review wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Delete Book review")
    public ResponseEntity<?> delete(@PathVariable long id) {
        return bookReviewService.delete(id)
                ? ResponseEntity.status(HttpStatus.NO_CONTENT).build() : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @RequestMapping(path = "/reviews/{id}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - proper execution of service", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book review wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Patch Book review")
    public ResponseEntity<BookReview> patch(@RequestBody Map<String, Object> bookReviewUpdates, @PathVariable("id") long id) {
        return bookReviewService.patch(bookReviewUpdates, id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

}
