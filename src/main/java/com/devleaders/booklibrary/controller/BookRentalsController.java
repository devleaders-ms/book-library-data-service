package com.devleaders.booklibrary.controller;

import com.devleaders.booklibrary.model.RentalsDto;
import com.devleaders.booklibrary.service.RentalsCrudService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@RequestMapping("/v1/api")
@Validated
@RequiredArgsConstructor
public class BookRentalsController {

    private final RentalsCrudService rentalsCrudService;

    @PostMapping(path = "/bookRentals", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class)})
    @ApiOperation(value = "Create Book rentals")
    public ResponseEntity<RentalsDto> create(@RequestBody RentalsDto bookRentals) {
        RentalsDto newBookRentals = rentalsCrudService.create(bookRentals);
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(newBookRentals);
    }

    @GetMapping(path = "/bookRentals", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class)})
    @ApiOperation(value = "Get all Book rentals")
    public ResponseEntity<List<RentalsDto>> getAll(@RequestParam(required = false) Long userId) {
        List<RentalsDto> bookRentals = Optional.ofNullable(userId)
                .map(rentalsCrudService::getBookRentalsByUserId)
                .orElseGet(rentalsCrudService::getAllBookRentals);
        return ResponseEntity.ok().body(bookRentals);
    }

    @GetMapping(path = "/bookRentals/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book rentals wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Read Book rentals")
    public ResponseEntity<RentalsDto> read(@PathVariable long id) {
        return rentalsCrudService.read(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }

    @DeleteMapping(path = "/bookRentals/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 204, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book rentals wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Delete Book rentals")
    public ResponseEntity<?> delete(@PathVariable long id) {
        return rentalsCrudService.delete(id)
                ? ResponseEntity.status(HttpStatus.NO_CONTENT).build() : ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    @PatchMapping(path = "/bookRentals/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiResponses(value = {@ApiResponse(code = 200, message = "Success - prawidłowe wywołanie usługi", response = ResponseStatus.class),
            @ApiResponse(code = 404, message = "Book rentals wasn't found", response = ResponseStatus.class)})
    @ApiOperation(value = "Update Book rentals")
    public ResponseEntity<RentalsDto> patch(@RequestBody Map<String, Object> rentalsBookUpdates,
                                            @PathVariable("id") long id) {
        return rentalsCrudService.patch(rentalsBookUpdates, id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).build());
    }
}
