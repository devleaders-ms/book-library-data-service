package com.devleaders.booklibrary.mapper;

import com.devleaders.booklibrary.model.Author;
import com.devleaders.booklibrary.model.AuthorDto;
import org.springframework.stereotype.Component;

@Component
public class AuthorMapper {

    public Author mapToEntity(AuthorDto authorDto) {
        return Author.builder()
                .name(authorDto.getName())
                .lastName(authorDto.getLastName())
                .build();
    }

    public AuthorDto mapToDto(Author author) {
        return AuthorDto.builder()
                .id(author.getId())
                .name(author.getName())
                .lastName(author.getLastName())
                .build();
    }

}
