package com.devleaders.booklibrary.mapper;

import com.devleaders.booklibrary.model.User;
import com.devleaders.booklibrary.model.UserDto;
import org.springframework.stereotype.Service;

@Service
public class UserMapper {

    public UserDto mapUserToDto(User user) {
        return UserDto.builder()
                .id(user.getId())
                .login(user.getLogin())
                .email(user.getEmail())
                .password(user.getPassword())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .roles(user.getRoles())
                .build();
    }

    public User mapDtoToUser(UserDto userDto) {
        return User.builder()
                .id(userDto.getId())
                .login(userDto.getLogin())
                .email(userDto.getEmail())
                .password(userDto.getPassword())
                .firstName(userDto.getFirstName())
                .lastName(userDto.getLastName())
                .build();
    }

}
