package com.devleaders.booklibrary.mapper;

import com.devleaders.booklibrary.model.Book;
import com.devleaders.booklibrary.model.BookReview;
import com.devleaders.booklibrary.model.BookReviewDto;
import com.devleaders.booklibrary.model.User;
import org.springframework.stereotype.Component;

@Component
public class BookReviewMapper {

    public BookReview mapToBookReview(BookReviewDto bookReviewDto, Book book, User user, Long id) {
        return BookReview.builder()
                .id(id)
                .user(user)
                .book(book)
                .content(bookReviewDto.getContent())
                .rate(bookReviewDto.getRate())
                .build();
    }

    public BookReviewDto mapToBookReviewDto(BookReview bookReview) {
        return BookReviewDto.builder()
                .content(bookReview.getContent())
                .rate(bookReview.getRate())
                .userId(bookReview.getUser().getId())
                .bookId(bookReview.getBook().getId())
                .build();
    }

}
