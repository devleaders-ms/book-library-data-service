package com.devleaders.booklibrary.mapper;

import com.devleaders.booklibrary.model.BookItem;
import com.devleaders.booklibrary.model.Rentals;
import com.devleaders.booklibrary.model.RentalsDto;
import com.devleaders.booklibrary.model.User;
import org.springframework.stereotype.Service;

@Service
public class RentalsMapper {

    public RentalsDto mapBookRentalsToDto(Rentals rentals) {
        return RentalsDto.builder()
                .id(rentals.getId())
                .userId(rentals.getUser().getId())
                .bookItemId(rentals.getBookItem().getId())
                .rentDate(rentals.getRentDate())
                .dueDate(rentals.getDueDate())
                .returnDate(rentals.getDueDate())
                .build();
    }

    public Rentals mapDtoToBookRentals(RentalsDto rentalsDto, BookItem bookItem, User user) {
        return Rentals.builder()
                .bookItem(bookItem)
                .user(user)
                .rentDate(rentalsDto.getRentDate())
                .dueDate(rentalsDto.getDueDate())
                .returnDate(rentalsDto.getReturnDate())
                .build();
    }

}