package com.devleaders.booklibrary.mapper;

import com.devleaders.booklibrary.model.BookItem;
import com.devleaders.booklibrary.model.Penalty;
import com.devleaders.booklibrary.model.PenaltyDto;
import com.devleaders.booklibrary.model.User;
import org.springframework.stereotype.Service;

@Service
public class PenaltyMapper {

    public Penalty mapToPenalty(PenaltyDto penaltyDto, BookItem bookItem, User user, Long id) {
        return Penalty.builder()
                .id(id)
                .user(user)
                .bookItem(bookItem)
                .startedAt(penaltyDto.getStartedAt())
                .payedAt(penaltyDto.getPayedAt())
                .status(penaltyDto.getStatus())
                .charge(penaltyDto.getCharge())
                .build();
    }

    public PenaltyDto mapToPenaltyDto(Penalty penalty) {
        return PenaltyDto.builder()
                .bookItemId(penalty.getBookItem().getId())
                .userId(penalty.getUser().getId())
                .startedAt(penalty.getStartedAt())
                .payedAt(penalty.getPayedAt())
                .status(penalty.getStatus())
                .charge(penalty.getCharge())
                .build();
    }
}
