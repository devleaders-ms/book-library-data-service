package com.devleaders.booklibrary.mapper;

import com.devleaders.booklibrary.model.Author;
import com.devleaders.booklibrary.model.AuthorDto;
import com.devleaders.booklibrary.model.Book;
import com.devleaders.booklibrary.model.BookDto;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class BookMapper {

    private final AuthorMapper authorMapper;

    public Book mapToEntity(BookDto bookDto, List<Author> authors) {
        return Book.builder()
                .id(null)
                .title(bookDto.getTitle())
                .isbn(bookDto.getIsbn())
                .description(bookDto.getDescription())
                .category(bookDto.getCategory())
                .image(bookDto.getImage())
                .pages(bookDto.getPages())
                .authors(authors)
                .build();
    }

    public BookDto mapToDto(Book book) {
        List<AuthorDto> authors = book.getAuthors()
                .stream()
                .map(authorMapper::mapToDto)
                .collect(Collectors.toList());

        return BookDto.builder()
                .id(book.getId())
                .title(book.getTitle())
                .isbn(book.getIsbn())
                .description(book.getDescription())
                .authors(authors)
                .category(book.getCategory())
                .image(book.getImage())
                .pages(book.getPages())
                .build();
    }

}
