package com.devleaders.booklibrary.mapper;

import com.devleaders.booklibrary.model.BookItem;
import com.devleaders.booklibrary.model.Reservation;
import com.devleaders.booklibrary.model.ReservationDto;
import com.devleaders.booklibrary.model.User;
import org.springframework.stereotype.Service;

@Service
public class ReservationMapper {
    public Reservation mapToReservation(ReservationDto reservationDto, BookItem bookItem, User user, Long id) {
        return Reservation
                .builder()
                .id(id)
                .bookItem(bookItem)
                .user(user)
                .reservationDate(reservationDto.getReservationDate())
                .status(reservationDto.getStatus())
                .build();
    }

    public ReservationDto mapToDto(Reservation reservation) {
        return ReservationDto
                .builder()
                .bookItemId(reservation.getBookItem().getId())
                .userId(reservation.getUser().getId())
                .reservationDate(reservation.getReservationDate())
                .status(reservation.getStatus())
                .build();
    }
}
