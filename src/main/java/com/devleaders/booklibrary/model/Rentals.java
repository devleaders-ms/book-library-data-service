package com.devleaders.booklibrary.model;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;


@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Rentals {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @JsonProperty("bookItemId")
    @JsonIdentityReference(alwaysAsId = true)
    @ManyToOne(cascade = CascadeType.PERSIST)
    private BookItem bookItem;

    @NotNull
    @JsonProperty("userId")
    @JsonIdentityReference(alwaysAsId = true)
    @ManyToOne
    private User user;

    @NonNull
    private LocalDate rentDate;

    @NonNull
    @Future
    private LocalDate dueDate;

    @Future
    private LocalDate returnDate;

}
