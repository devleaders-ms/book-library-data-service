package com.devleaders.booklibrary.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class ReservationDto {

    private Long id;
    private Long bookItemId;
    private Long userId;
    private LocalDateTime reservationDate;
    private ReservationStatus status;
}
