package com.devleaders.booklibrary.model;

public enum ReservationStatus {
    AWAITING,
    READY,
    CANCELLED
}
