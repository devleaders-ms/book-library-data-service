package com.devleaders.booklibrary.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AuthorDto {
    private Long id;
    private String name;
    private String lastName;
}
