package com.devleaders.booklibrary.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class BookReviewDto {
    private Long id;
    private Long bookId;
    private Long userId;
    private int rate;
    private String content;
}
