package com.devleaders.booklibrary.model;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reservation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @JsonProperty("bookItemId")
    @JsonIdentityReference(alwaysAsId = true)
    @OneToOne(cascade = CascadeType.PERSIST)
    private BookItem bookItem;

    @NotNull
    @JsonProperty("userId")
    @JsonIdentityReference(alwaysAsId = true)
    @OneToOne(cascade = CascadeType.PERSIST)
    private User user;

    @NonNull
    private LocalDateTime reservationDate;

    @NonNull
    @Enumerated(EnumType.STRING)
    private ReservationStatus status;

}
