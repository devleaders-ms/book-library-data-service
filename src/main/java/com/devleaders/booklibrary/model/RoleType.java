package com.devleaders.booklibrary.model;

public enum RoleType {
    ROLE_USER,
    ROLE_LIBRARIAN,
    ROLE_ADMIN
}
