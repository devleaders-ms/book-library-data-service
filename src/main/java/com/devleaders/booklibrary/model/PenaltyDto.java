package com.devleaders.booklibrary.model;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
@Builder
public class PenaltyDto {
    private long id;
    private long userId;
    private long bookItemId;
    private Date startedAt;
    private PenaltyStatus status;
    private Date payedAt;
    private BigDecimal charge;
}
