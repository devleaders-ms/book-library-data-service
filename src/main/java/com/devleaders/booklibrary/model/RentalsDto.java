package com.devleaders.booklibrary.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Builder
@Data
public class RentalsDto {
    private LocalDate dueDate;
    private Long id;
    @NotNull
    private Long bookItemId;
    @NotNull
    private Long userId;
    private LocalDate rentDate;
    private LocalDate returnDate;
}
