package com.devleaders.booklibrary.model;

public enum PenaltyStatus {
    CHARGED,
    PAID
}
