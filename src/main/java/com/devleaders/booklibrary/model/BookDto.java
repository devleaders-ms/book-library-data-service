package com.devleaders.booklibrary.model;

import lombok.Builder;
import lombok.Data;

import java.util.Collection;

@Data
@Builder
public class BookDto {
    private long id;
    private String title;
    private String isbn;
    private String description;
    private Collection<AuthorDto> authors;
    private String category;
    private String image;
    private int pages;
}
