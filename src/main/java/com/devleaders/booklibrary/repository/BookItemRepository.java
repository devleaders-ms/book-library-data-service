package com.devleaders.booklibrary.repository;

import com.devleaders.booklibrary.model.BookItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface BookItemRepository extends JpaRepository<BookItem, Long>, QuerydslPredicateExecutor<BookItem> {
}
