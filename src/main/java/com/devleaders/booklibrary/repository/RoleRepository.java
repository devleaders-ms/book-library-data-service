package com.devleaders.booklibrary.repository;

import com.devleaders.booklibrary.model.Role;
import com.devleaders.booklibrary.model.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, QuerydslPredicateExecutor<Role> {
    Optional<Role> findRoleByRoleType(RoleType roleType);
}