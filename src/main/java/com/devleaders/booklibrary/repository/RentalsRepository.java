package com.devleaders.booklibrary.repository;

import com.devleaders.booklibrary.model.Rentals;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface RentalsRepository extends JpaRepository<Rentals, Long>, QuerydslPredicateExecutor<Rentals> {
}
