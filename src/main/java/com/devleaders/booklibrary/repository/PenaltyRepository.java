package com.devleaders.booklibrary.repository;

import com.devleaders.booklibrary.model.Penalty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PenaltyRepository extends JpaRepository<Penalty, Long>, QuerydslPredicateExecutor<Penalty> {
}
