package com.devleaders.booklibrary.repository;

import com.devleaders.booklibrary.model.BookReview;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface BookReviewRepository extends JpaRepository<BookReview, Long>, QuerydslPredicateExecutor<BookReview> {
}
