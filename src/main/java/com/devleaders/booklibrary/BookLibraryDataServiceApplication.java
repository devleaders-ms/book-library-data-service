package com.devleaders.booklibrary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookLibraryDataServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(BookLibraryDataServiceApplication.class, args);
    }
}
