package com.devleaders.booklibrary.service;

import com.devleaders.booklibrary.error.BookNotFoundException;
import com.devleaders.booklibrary.model.BookItem;
import com.devleaders.booklibrary.model.QBookItem;
import com.devleaders.booklibrary.repository.BookItemRepository;
import com.devleaders.booklibrary.repository.BookRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@AllArgsConstructor

public class BookItemCrudService {

    @PersistenceContext
    private final EntityManager entityManager;
    private final BookItemRepository bookItemRepository;
    private final BookRepository bookRepository;
    private final ObjectMapper objectMapper;

    public List<BookItem> getAllBookItems() {
        return bookItemRepository.findAll();
    }

    public List<BookItem> getBookItemsByBookId(Long bookId) {
        final QBookItem qBookItem = QBookItem.bookItem;
        JPAQuery<Object> query = new JPAQuery<>(entityManager);

        return Optional.of(bookRepository.existsById(bookId))
                .filter(exist -> exist)
                .map(__ -> query.select(qBookItem)
                        .from(qBookItem)
                        .where(qBookItem.bookId.eq(bookId))
                        .fetch())
                .orElseThrow(() -> new BookNotFoundException(bookId));
    }

    public Optional<BookItem> read(Long id) {
        final QBookItem qBookItem = QBookItem.bookItem;
        JPAQuery<Object> query = new JPAQuery<>(entityManager);

        BookItem book = query.select(qBookItem)
                .from(qBookItem)
                .where(qBookItem.id.eq(id))
                .fetchOne();
        return Optional.ofNullable(book);
    }

    public Optional<BookItem> update(BookItem bookItem, long id) {
        return bookItemRepository.findById(id)
                .map(__ -> bookItemRepository.save(bookItem));
    }

    public BookItem create(BookItem bookItem) {
        return bookItemRepository.save(bookItem);
    }

    public boolean delete(Long id) {
        return Optional.of(bookItemRepository.existsById(id))
                .filter(exist -> exist)
                .map(__ -> {
                    bookItemRepository.deleteById(id);
                    return true;
                })
                .orElse(false);
    }

    public Optional<BookItem> patch(Map<String, Object> bookChanges, long id) {
        return bookItemRepository.findById(id)
                .map(bookItem -> objectMapper.convertValue(bookItem, new TypeReference<Map<String, Object>>() {}))
                .map(bookItemToUpdateMap -> {
                    bookChanges.forEach(bookItemToUpdateMap::put);
                    return bookItemToUpdateMap;
                })
                .map(bookItemMap -> objectMapper.convertValue(bookItemMap, BookItem.class))
                .map(bookItemRepository::save);
    }

    public List<BookItem> getAvailableBookItems(long bookId, Boolean isAvailable) {
        final QBookItem qBookItem = QBookItem.bookItem;
        JPAQuery<Object> query = new JPAQuery<>(entityManager);

        return Optional.of(bookRepository.existsById(bookId))
                .filter(exist -> exist)
                .map(__ -> query.select(qBookItem)
                        .from(qBookItem)
                        .where(qBookItem.bookId.eq(bookId))
                        .where(qBookItem.isAvailable.eq(isAvailable))
                        .fetch())
                .orElseThrow(() -> new BookNotFoundException(bookId));
    }
}
