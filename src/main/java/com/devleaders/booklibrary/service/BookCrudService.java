package com.devleaders.booklibrary.service;

import com.devleaders.booklibrary.error.BookAlreadyExistsException;
import com.devleaders.booklibrary.mapper.BookMapper;
import com.devleaders.booklibrary.model.Author;
import com.devleaders.booklibrary.model.Book;
import com.devleaders.booklibrary.model.BookDto;
import com.devleaders.booklibrary.model.QAuthor;
import com.devleaders.booklibrary.model.QBook;
import com.devleaders.booklibrary.repository.BookRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BookCrudService {

    @PersistenceContext
    private final EntityManager entityManager;
    private final BookRepository bookRepository;
    private final AuthorService authorService;
    private final ObjectMapper objectMapper;
    private final BookMapper bookMapper;

    public List<BookDto> getAllBooks() {
        return bookRepository.findAll().stream()
                .map(bookMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public Optional<BookDto> read(Long id) {
        final QBook qBook = QBook.book;
        JPAQuery<Object> query = new JPAQuery<>(entityManager);

        Book book = query.select(qBook)
                .from(qBook)
                .where(qBook.id.eq(id))
                .fetchOne();
        return Optional.ofNullable(book)
                .map(bookMapper::mapToDto);
    }

    public Optional<BookDto> update(Book book, long id) {
        return bookRepository.findById(id)
                .map(__ -> bookRepository.save(book))
                .map(bookMapper::mapToDto);
    }

    public BookDto create(BookDto bookDto) {
        return (BookDto) bookRepository.findByIsbn(bookDto.getIsbn())
                .map(x -> {
                    throw new BookAlreadyExistsException("Book with ISBN: " + bookDto.getIsbn() + " already exists");
                })
                .orElseGet(() -> {
                            List<Author> authors = authorService.getAuthorsEntities(bookDto.getAuthors());
                            Book bookToSave = bookMapper.mapToEntity(bookDto, authors);
                            Book book = bookRepository.save(bookToSave);
                            return bookMapper.mapToDto(book);
                        }
                );
    }

    public boolean delete(Long id) {
        return Optional.of(bookRepository.existsById(id))
                .filter(exist -> exist)
                .map(__ -> {
                    bookRepository.deleteById(id);
                    return true;
                })
                .orElse(false);
    }

    public Optional<BookDto> patch(Map<String, Object> bookChanges, long id) {
        return bookRepository.findById(id)
                .map(book -> objectMapper.convertValue(book, new TypeReference<Map<String, Object>>() {
                }))
                .map(bookToUpdateMap -> {
                    bookChanges.forEach(bookToUpdateMap::put);
                    return bookToUpdateMap;
                })
                .map(bookToUpdateMap -> objectMapper.convertValue(bookToUpdateMap, Book.class))
                .map(bookRepository::save)
                .map(bookMapper::mapToDto);
    }

    public List<BookDto> getBooksByAuthor(String authorName) {
        final QBook qBook = QBook.book;
        final QAuthor qAuthor = QAuthor.author;
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);

        BooleanExpression containsNameAndLastName = qAuthor.name
                .append(" ")
                .append(qAuthor.lastName)
                .containsIgnoreCase(authorName);

        BooleanExpression containsLastNameAndName = qAuthor.lastName
                .append(" ")
                .append(qAuthor.name)
                .containsIgnoreCase(authorName);

        return queryFactory.selectFrom(qBook)
                .innerJoin(qBook.authors, qAuthor)
                .on(containsNameAndLastName.or(containsLastNameAndName))
                .fetch()
                .stream()
                .map(bookMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public List<BookDto> getBooksByTitleLike(String title) {

        final QBook qBook = QBook.book;
        JPAQuery<Object> query = new JPAQuery<>(entityManager);

        return query.select(qBook)
                .from(qBook)
                .where(qBook.title.containsIgnoreCase(title))
                .fetch()
                .stream()
                .map(bookMapper::mapToDto)
                .collect(Collectors.toList());
    }

    public List<BookDto> getBooksByAuthorAndTitle(String author, String title) {
        final QBook qBook = QBook.book;
        final QAuthor qAuthor = QAuthor.author;
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);

        BooleanExpression containsNameAndLastName = qAuthor.name
                .append(" ")
                .append(qAuthor.lastName)
                .containsIgnoreCase(author);

        BooleanExpression containsLastNameAndName = qAuthor.lastName
                .append(" ")
                .append(qAuthor.name)
                .containsIgnoreCase(author);

        return queryFactory.select(qBook)
                .from(qBook)
                .where(qBook.title.containsIgnoreCase(title))
                .innerJoin(qBook.authors, qAuthor)
                .on(containsNameAndLastName.or(containsLastNameAndName))
                .fetch()
                .stream()
                .map(bookMapper::mapToDto)
                .collect(Collectors.toList());
    }
}
