package com.devleaders.booklibrary.service;

import com.devleaders.booklibrary.mapper.RentalsMapper;
import com.devleaders.booklibrary.model.BookItem;
import com.devleaders.booklibrary.model.QRentals;
import com.devleaders.booklibrary.model.Rentals;
import com.devleaders.booklibrary.model.RentalsDto;
import com.devleaders.booklibrary.model.User;
import com.devleaders.booklibrary.repository.BookItemRepository;
import com.devleaders.booklibrary.repository.RentalsRepository;
import com.devleaders.booklibrary.repository.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class RentalsCrudService {

    @PersistenceContext
    private final EntityManager entityManager;
    private final RentalsRepository rentalsRepository;
    private final ObjectMapper objectMapper;
    private final RentalsMapper rentalsMapper;
    private final UserRepository userRepository;
    private final BookItemRepository bookItemRepository;


    public List<RentalsDto> getAllBookRentals() {
        return rentalsRepository.findAll().stream()
                .map(rentalsMapper::mapBookRentalsToDto)
                .collect(Collectors.toList());
    }

    public List<RentalsDto> getBookRentalsByUserId(Long userId) {
        final QRentals qBookRentals = QRentals.rentals;
        final JPAQuery<Object> query = new JPAQuery<>(entityManager);

        return query.select(qBookRentals)
                .from(qBookRentals)
                .where(qBookRentals.user.id.eq(userId))
                .fetch()
                .stream()
                .map(rentalsMapper::mapBookRentalsToDto)
                .collect(Collectors.toList());

    }

    public Optional<RentalsDto> read(Long id) {
        final QRentals qBookRentals = QRentals.rentals;
        JPAQuery<Object> query = new JPAQuery<>(entityManager);

        Rentals rentals = query.select(qBookRentals)
                .from(qBookRentals)
                .where(qBookRentals.id.eq(id))
                .fetchOne();

        return Optional.ofNullable(rentals)
                .map(rentalsMapper::mapBookRentalsToDto);
    }

    public RentalsDto create(RentalsDto rentalsDto) {
        BookItem bookItem = bookItemRepository.findById(rentalsDto.getBookItemId())
                .orElseThrow(RuntimeException::new);
        User user = userRepository.findById(rentalsDto.getUserId())
                .orElseThrow(RuntimeException::new);
        Rentals rentals = rentalsMapper.mapDtoToBookRentals(rentalsDto, bookItem, user);
        Rentals rentalsEntity = rentalsRepository.save(rentals);
        return rentalsMapper.mapBookRentalsToDto(rentalsEntity);
    }

    public boolean delete(Long id) {
        return Optional.of(rentalsRepository.existsById(id))
                .filter(exist -> exist)
                .map(__ -> {
                    rentalsRepository.deleteById(id);
                    return true;
                })
                .orElse(false);
    }

    public Optional<RentalsDto> patch(Map<String, Object> bookChanges, long id) {
        return rentalsRepository.findById(id)
                .map(bookRentals -> objectMapper.convertValue(bookRentals, new TypeReference<Map<String, Object>>() {
                }))
                .map(bookRentalsToUpdateMap -> {
                    bookChanges.forEach(bookRentalsToUpdateMap::put);
                    return bookRentalsToUpdateMap;
                })
                .map(bookRentalsToUpdateMap -> objectMapper.convertValue(bookRentalsToUpdateMap, Rentals.class))
                .map(rentalsRepository::save)
                .map(rentalsMapper::mapBookRentalsToDto);
    }

}
