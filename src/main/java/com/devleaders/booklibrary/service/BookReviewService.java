package com.devleaders.booklibrary.service;

import com.devleaders.booklibrary.error.UserOrBookDoesNotExistException;
import com.devleaders.booklibrary.mapper.BookReviewMapper;
import com.devleaders.booklibrary.model.Book;
import com.devleaders.booklibrary.model.BookReview;
import com.devleaders.booklibrary.model.BookReviewDto;
import com.devleaders.booklibrary.model.QBookReview;
import com.devleaders.booklibrary.model.User;
import com.devleaders.booklibrary.repository.BookRepository;
import com.devleaders.booklibrary.repository.BookReviewRepository;
import com.devleaders.booklibrary.repository.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookReviewService {

    @PersistenceContext
    private final EntityManager entityManager;
    private final BookReviewRepository bookReviewRepository;
    private final BookRepository bookRepository;
    private final BookReviewMapper bookReviewMapper;
    private final UserRepository userRepository;
    private final ObjectMapper objectMapper;

    public BookReview create(BookReviewDto bookReviewDto) {
        Book book = bookRepository.findById(bookReviewDto.getBookId())
                .orElseThrow(() -> new UserOrBookDoesNotExistException("Book with id " + bookReviewDto.getBookId() + " does not exist"));
        User user = userRepository.findById(bookReviewDto.getUserId())
                .orElseThrow(() -> new UserOrBookDoesNotExistException("User with id " + bookReviewDto.getUserId() + " does not exist"));

        BookReview bookReview = bookReviewMapper.mapToBookReview(bookReviewDto, book, user, null);
        return bookReviewRepository.save(bookReview);
    }

    public List<BookReview> getAllByBookId(long bookId) {
        final QBookReview qBookReview = QBookReview.bookReview;
        JPAQuery<Object> query = new JPAQuery<>(entityManager);

        return query.select(qBookReview)
                .from(qBookReview)
                .where(qBookReview.book.id.eq(bookId))
                .fetch();
    }

    public List<BookReview> getAllByUserId(long userId) {
        final QBookReview qBookReview = QBookReview.bookReview;
        JPAQuery<Object> query = new JPAQuery<>(entityManager);

        return query.select(qBookReview)
                .from(qBookReview)
                .where(qBookReview.user.id.eq(userId))
                .fetch();
    }

    public Optional<BookReview> getById(long bookReviewId) {
        return bookReviewRepository.findById(bookReviewId);
    }

    public Optional<BookReview> update(BookReviewDto bookReviewDto, Long id) {
        BookReview bookReview = null;
        if (bookReviewRepository.existsById(id)) {
            Book book = bookRepository.findById(bookReviewDto.getBookId())
                    .orElseThrow(() -> new UserOrBookDoesNotExistException("Book with id " + bookReviewDto.getBookId() + " does not exist"));
            User user = userRepository.findById(bookReviewDto.getUserId())
                    .orElseThrow(() -> new UserOrBookDoesNotExistException("User with id " + bookReviewDto.getUserId() + " does not exist"));

            BookReview bookReviewToUpdate = bookReviewMapper.mapToBookReview(bookReviewDto, book, user, id);
            bookReview = bookReviewRepository.save(bookReviewToUpdate);
        }
        return Optional.ofNullable(bookReview);
    }

    public boolean delete(long id) {
        return bookReviewRepository.findById(id)
                .map(bookReview -> {
                    bookReviewRepository.delete(bookReview);
                    return true;
                })
                .orElse(false);
    }

    public Optional<BookReview> patch(Map<String, Object> bookReviewUpdates, long id) {
        Optional<BookReview> bookReview = Optional.empty();
        if (bookReviewRepository.existsById(id)) {
            bookReview = bookReviewRepository.findById(id)
                    .map(bookReviewMapper::mapToBookReviewDto)
                    .map(bookReviewDto -> objectMapper.convertValue(bookReviewDto, new TypeReference<Map<String, Object>>() {
                    }))
                    .map(bookReviewDtoToUpdateMap -> {
                        bookReviewUpdates.forEach(bookReviewDtoToUpdateMap::put);
                        return bookReviewDtoToUpdateMap;
                    })
                    .map(bookReviewToUpdateMap -> objectMapper.convertValue(bookReviewToUpdateMap, BookReviewDto.class))
                    .map(bookReviewDto -> convertToBookReview(bookReviewDto, id))
                    .map(bookReviewRepository::save);
        }
        return bookReview;
    }

    private BookReview convertToBookReview(BookReviewDto bookReviewDto, long id) {
        Book book = bookRepository.findById(bookReviewDto.getBookId())
                .orElseThrow(() -> new UserOrBookDoesNotExistException("Book with id " + bookReviewDto.getBookId() + " does not exist"));
        User user = userRepository.findById(bookReviewDto.getUserId())
                .orElseThrow(() -> new UserOrBookDoesNotExistException("User with id " + bookReviewDto.getUserId() + " does not exist"));

        return bookReviewMapper.mapToBookReview(bookReviewDto, book, user, id);
    }

}
