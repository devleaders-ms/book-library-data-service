package com.devleaders.booklibrary.service;

import com.devleaders.booklibrary.model.QUser;
import com.devleaders.booklibrary.model.Role;
import com.devleaders.booklibrary.model.User;
import com.devleaders.booklibrary.repository.RoleRepository;
import com.devleaders.booklibrary.repository.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.devleaders.booklibrary.model.RoleType.ROLE_USER;

@Service
@RequiredArgsConstructor
public class UserCrudService {
    @PersistenceContext
    private final EntityManager entityManager;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final ObjectMapper objectMapper;

    public Optional<User> read(long id) {
        final QUser qUser = QUser.user;
        final JPAQuery<Object> query = new JPAQuery<>(entityManager);

        User user = query.select(qUser)
                .from(qUser)
                .where(qUser.id.eq(id))
                .fetchOne();
        return Optional.ofNullable(user);
    }

    public Optional<User> read(String login) {
        final QUser qUser = QUser.user;
        final JPAQuery<Object> query = new JPAQuery<>(entityManager);

        User user = query.select(qUser)
                .from(qUser)
                .where(qUser.login.eq(login))
                .fetchOne();
        return Optional.ofNullable(user);
    }

    public List<User> getUsersByFullName(String fullName) {
        final QUser qUser = QUser.user;
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);


        if (fullName != null && !fullName.isEmpty()) {
            StringPath userFirstName = qUser.firstName;
            StringPath userLastName = qUser.lastName;
            StringExpression userFullName = qUser.firstName.append(" ").append(qUser.lastName);

            return queryFactory.selectFrom(qUser)
                    .where(userFullName.containsIgnoreCase(fullName).or(userFirstName.containsIgnoreCase(fullName)).or(userLastName.containsIgnoreCase(fullName)))
                    .fetch();
        }

        return queryFactory.selectFrom(qUser)
                .fetch();
    }

    public Optional<User> update(User user) {
        return userRepository.findById(user.getId())
                .map(__ -> userRepository.save(user));
    }

    public User create(User user) {
        Role role = roleRepository.findRoleByRoleType(ROLE_USER)
                .orElseThrow(() -> new RuntimeException("Role not found"));
        user.setRoles(Set.of(role));
        return userRepository.save(user);
    }

    public boolean delete(long id) {
        return Optional.of(userRepository.existsById(id))
                .filter(exist -> exist)
                .map(__ -> {
                    userRepository.deleteById(id);
                    return true;
                })
                .orElse(false);
    }

    public Optional<User> patch(Map<String, String> userChanges, long id) {
        return userRepository.findById(id)
                .map(user -> objectMapper.convertValue(user, new TypeReference<Map<String, Object>>() {
                }))
                .map(userToUpdateMap -> {
                    userChanges.forEach(userToUpdateMap::put);
                    return userToUpdateMap;
                })
                .map(userToUpdateMap -> objectMapper.convertValue(userToUpdateMap, User.class))
                .map(userRepository::save);
    }
}
