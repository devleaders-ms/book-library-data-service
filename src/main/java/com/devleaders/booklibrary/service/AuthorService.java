package com.devleaders.booklibrary.service;

import com.devleaders.booklibrary.mapper.AuthorMapper;
import com.devleaders.booklibrary.model.Author;
import com.devleaders.booklibrary.model.AuthorDto;
import com.devleaders.booklibrary.model.QAuthor;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthorService {

    @PersistenceContext
    private final EntityManager entityManager;
    private final AuthorMapper authorMapper;

    public List<Author> getAuthorsEntities(Collection<AuthorDto> authorsDto) {
        return authorsDto.stream()
                .map(authorMapper::mapToEntity)
                .map(authorEntity ->
                        findAuthorByNameAndLastName(authorEntity.getName(), authorEntity.getLastName())
                                .orElse(authorEntity)
                )
                .collect(Collectors.toList());
    }

    private Optional<Author> findAuthorByNameAndLastName(String name, String surname) {
        final QAuthor qAuthor = QAuthor.author;
        JPAQueryFactory queryFactory = new JPAQueryFactory(entityManager);
        return Optional.ofNullable(queryFactory.selectFrom(qAuthor)
                .where(qAuthor.name.eq(name).and(qAuthor.lastName.eq(surname)))
                .fetchFirst()
        );
    }

}
