package com.devleaders.booklibrary.service;

import com.devleaders.booklibrary.error.BookItemDoesNotExistException;
import com.devleaders.booklibrary.error.UserDoesNotExistException;
import com.devleaders.booklibrary.error.UserDoesNotExistExeption;
import com.devleaders.booklibrary.mapper.ReservationMapper;
import com.devleaders.booklibrary.model.BookItem;
import com.devleaders.booklibrary.model.QReservation;
import com.devleaders.booklibrary.model.Reservation;
import com.devleaders.booklibrary.model.ReservationDto;
import com.devleaders.booklibrary.model.User;
import com.devleaders.booklibrary.repository.BookItemRepository;
import com.devleaders.booklibrary.repository.ReservationRepository;
import com.devleaders.booklibrary.repository.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ReservationsService {

    @PersistenceContext
    private final EntityManager entityManager;
    private final ReservationRepository reservationRepository;
    private final ObjectMapper objectMapper;
    private final BookItemRepository bookItemRepository;
    private final UserRepository userRepository;
    private final ReservationMapper reservationMapper;

    public List<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    public List<Reservation> getReservationsByUserId(Long userId) {
        final QReservation qReservation = QReservation.reservation;
        final JPAQuery<Object> query = new JPAQuery<>(entityManager);

        return Optional.of(userRepository.existsById(userId))
                .filter(exist -> exist)
                .map(__ -> query.select(qReservation)
                        .from(qReservation)
                        .where(qReservation.user.id.eq(userId))
                        .fetch())
                .orElseThrow(()-> new UserDoesNotExistException(userId));
    }

    public Optional<Reservation> read(Long id) {
        final QReservation qReservation = QReservation.reservation;
        JPAQuery<Object> query = new JPAQuery<>(entityManager);

        Reservation reservation = query.select(qReservation)
                .from(qReservation)
                .where(qReservation.id.eq(id))
                .fetchOne();
        return Optional.ofNullable(reservation);
    }

    public Optional<Reservation> update(ReservationDto reservationDto, long id) {
        BookItem bookItem = bookItemRepository.findById(reservationDto.getBookItemId())
                .orElseThrow(() -> new BookItemDoesNotExistException(reservationDto.getBookItemId()));
        User user = userRepository.findById(reservationDto.getUserId())
                .orElseThrow(() -> new UserDoesNotExistExeption(reservationDto.getUserId()));
        Reservation reservation = reservationMapper.mapToReservation(reservationDto, bookItem, user, id);
        return reservationRepository.findById(id)
                .map(__ -> reservationRepository.save(reservation));
    }

    public Reservation create(ReservationDto reservationDto) {
        BookItem bookItem = bookItemRepository.findById(reservationDto.getBookItemId())
                .orElseThrow(() -> new BookItemDoesNotExistException(reservationDto.getBookItemId()));
        User user = userRepository.findById(reservationDto.getUserId())
                .orElseThrow(() -> new UserDoesNotExistExeption(reservationDto.getUserId()));
        Reservation reservation = reservationMapper.mapToReservation(reservationDto, bookItem, user, null);
        return reservationRepository.save(reservation);
    }

    public boolean delete(Long id) {
        return Optional.of(reservationRepository.existsById(id))
                .filter(exist -> exist)
                .map(__ -> {
                    reservationRepository.deleteById(id);
                    return true;
                })
                .orElse(false);
    }

    public Optional<Reservation> patch(Map<String, Object> reservationChanges, long id) {
        Optional<Reservation> reservation = Optional.empty();
        if (reservationRepository.existsById(id)) {
            reservation = reservationRepository.findById(id)
                    .map(reservationMapper::mapToDto)
                    .map(reservationDto -> objectMapper.convertValue(reservationDto, new TypeReference<Map<String, Object>>() {
                    }))
                    .map(reservationDtoMap -> {
                        reservationChanges.forEach(reservationDtoMap::put);
                        return reservationDtoMap;
                    })
                    .map(reservationDtoMap -> objectMapper.convertValue(reservationDtoMap, ReservationDto.class))
                    .map(reservationDto -> convertToReservation(reservationDto, id))
                    .map(reservationRepository::save);
        }
        return reservation;
    }

    private Reservation convertToReservation(ReservationDto reservationDto, long id) {
        BookItem bookItem = bookItemRepository.findById(reservationDto.getBookItemId())
                .orElseThrow(() -> new BookItemDoesNotExistException(reservationDto.getBookItemId()));
        User user = userRepository.findById(reservationDto.getUserId())
                .orElseThrow(() -> new UserDoesNotExistExeption(reservationDto.getUserId()));
        return reservationMapper.mapToReservation(reservationDto, bookItem, user, id);
    }

}


