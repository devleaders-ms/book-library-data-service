package com.devleaders.booklibrary.service;

import com.devleaders.booklibrary.error.UserOrBookDoesNotExistException;
import com.devleaders.booklibrary.error.UserOrBookItemDoesNotExistException;
import com.devleaders.booklibrary.mapper.PenaltyMapper;
import com.devleaders.booklibrary.model.BookItem;
import com.devleaders.booklibrary.model.Penalty;
import com.devleaders.booklibrary.model.PenaltyDto;
import com.devleaders.booklibrary.model.QPenalty;
import com.devleaders.booklibrary.model.User;
import com.devleaders.booklibrary.repository.BookItemRepository;
import com.devleaders.booklibrary.repository.PenaltyRepository;
import com.devleaders.booklibrary.repository.UserRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.querydsl.jpa.impl.JPAQuery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PenaltyService {

    @PersistenceContext
    private final EntityManager entityManager;
    private final PenaltyRepository penaltyRepository;
    private final UserRepository userRepository;
    private final BookItemRepository bookItemRepository;
    private final ObjectMapper objectMapper;
    private final PenaltyMapper penaltyMapper;

    public Penalty create(PenaltyDto penaltyDto) {
        User user = userRepository.findById(penaltyDto.getUserId())
                .orElseThrow(() -> new UserOrBookItemDoesNotExistException("User with id " + penaltyDto.getUserId() + " does not exist!"));
        BookItem bookItem = bookItemRepository.findById(penaltyDto.getBookItemId())
                .orElseThrow(() -> new UserOrBookItemDoesNotExistException("BookItem with id " + penaltyDto.getBookItemId() + " does not exist!"));
        Penalty penalty = penaltyMapper.mapToPenalty(penaltyDto, bookItem, user, null);
        return penaltyRepository.save(penalty);
    }

    public List<Penalty> readAll() {
        return penaltyRepository.findAll();
    }

    public Optional<Penalty> readById(long id) {
        return penaltyRepository.findById(id);
    }

    public Optional<List<Penalty>> readAllByUserId(long userId) {
        final QPenalty qPenalty = QPenalty.penalty;
        JPAQuery<Object> query = new JPAQuery<>(entityManager);

        List<Penalty> penalties = query.select(qPenalty)
                .from(qPenalty)
                .where(qPenalty.user.id.eq(userId))
                .fetch();
        return Optional.of(penalties);
    }

    public Optional<List<Penalty>> readAllByBookItemId(long bookItemId) {
        final QPenalty qPenalty = QPenalty.penalty;
        JPAQuery<Object> query = new JPAQuery<>(entityManager);

        List<Penalty> penalties = query.select(qPenalty)
                .from(qPenalty)
                .where(qPenalty.bookItem.id.eq(bookItemId))
                .fetch();
        return Optional.of(penalties);
    }

    public Optional<Boolean> delete(long id) {
        Boolean deleted = penaltyRepository.findById(id)
                .map(penalty -> {
                    penaltyRepository.delete(penalty);
                    return true;
                })
                .orElse(false);
        return Optional.of(deleted);
    }

    public Optional<Penalty> update(PenaltyDto penaltyDto, Long id) {
        Penalty penalty = null;
        if (penaltyRepository.existsById(id)) {
            BookItem bookItem = bookItemRepository.findById(penaltyDto.getBookItemId())
                    .orElseThrow(() -> new UserOrBookItemDoesNotExistException("BookItem with id " + penaltyDto.getBookItemId() + " does not exist"));
            User user = userRepository.findById(penaltyDto.getUserId())
                    .orElseThrow(() -> new UserOrBookItemDoesNotExistException("User with id " + penaltyDto.getUserId() + " does not exist"));

            Penalty penaltyToUpdate = penaltyMapper.mapToPenalty(penaltyDto, bookItem, user, id);
            penalty = penaltyRepository.save(penaltyToUpdate);
        }
        return Optional.ofNullable(penalty);
    }

    public Optional<Penalty> patch(Map<String, Object> penaltyUpdates, long id) {
        Optional<Penalty> penalty = Optional.empty();
        if (penaltyRepository.existsById(id)) {
            penalty = penaltyRepository.findById(id)
                    .map(penaltyMapper::mapToPenaltyDto)
                    .map(penaltyDto -> objectMapper.convertValue(penaltyDto, new TypeReference<Map<String, Object>>() {
                    }))
                    .map(penaltyToUpdateMap -> {
                        penaltyUpdates.forEach(penaltyToUpdateMap::put);
                        return penaltyToUpdateMap;
                    })
                    .map(penaltyToUpdateMap -> objectMapper.convertValue(penaltyToUpdateMap, PenaltyDto.class))
                    .map(penaltyDto -> mapToPenalty(penaltyDto, id))
                    .map(penaltyRepository::save);
        }
        return penalty;
    }

    private Penalty mapToPenalty(PenaltyDto penaltyDto, long id) {
        BookItem bookItem = bookItemRepository.findById(penaltyDto.getBookItemId())
                .orElseThrow(() -> new UserOrBookDoesNotExistException("BookItem with id " + penaltyDto.getBookItemId() + " does not exist"));
        User user = userRepository.findById(penaltyDto.getUserId())
                .orElseThrow(() -> new UserOrBookDoesNotExistException("User with id " + penaltyDto.getUserId() + " does not exist"));

        return penaltyMapper.mapToPenalty(penaltyDto, bookItem, user, id);
    }

}
